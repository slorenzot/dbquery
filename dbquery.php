<?php
//
// Clase base para la herencia de los generados de consultas
//
abstract class Query {
	var $driver = '';

	public function addSubQuery($query) {
		if($this instanceof SelectQuery) {
			array_push($this->_subqueries, new SelectQuery());
		}
		// TODO: implementar sub consultas
		return $this;
	}

	public function addColumn($name, $alias = '') {
		return implode('.', explode('.', $name));
	}

	public function addValue($value) {
		$type = gettype($value);
		return ($type=='string')?sprintf('\'%s\'',$value):$value;
	}

	public function addCondition($condition, $valuesA, $valuesB = NULL, $isRestriction = false) {
		$_allowConditions = array(
			'isequal',
			'islike',
			'isilike',
			'isin',
			'islargerthan',
			'islargereqthan',
			'islessthan',
			'islesseqthan',
			'isnull',
			'isnotnull',
			'istrue',
			'isfalse'
		);
		$isEqual_expr           = "%s = %s"; //No distingue mayusculas y minusculas
		$isLike_expr            = "%s = '%s'"; //Distingue mayusculas y minusculas
		$isILike_expr           = "UPPER(%s) LIKE UPPER(%s)"; //No distingue mayusculas y minusculas
		$isIn_expr              = "(%s) IN(%s)";
		$isLargerThan_expr      = "%s > %s";
		$isLargerEqThan_expr    = "%s >= %s";
		$isLessThan_expr        = "%s < %s";
		$isLessEqThan_expr      = "%s >= %s";
		$isNull_expr            = "%s IS NULL";
		$isNotNull_expr         = "NOT %s IS NULL";
		$isTrue_expr            = "%s";
		$isFalse_expr           = "NOT %s";

		if(in_array(strtolower($condition), $_allowConditions)) {
			switch(strtolower($condition)) {
				case 'isequal':
					// if(!is_array($valuesA) && !is_array($valuesB))
					// 	array_push($this->_conditions, array('isequal', $isEqual_expr, array($valuesA, $valuesB)));
					if(!is_array($valuesA) && !is_array($valuesB))
						array_push($this->_conditions, array('isequal', $isEqual_expr,
							array($this->addColumn($valuesA), $this->addValue($valuesB), ($valuesB instanceof SelectQuery))));
					break;

				case 'islike':
					if(!is_array($valuesA) && !is_array($valuesB))
						array_push($this->_conditions, array('islike', $isLike_expr, array($valuesA, $valuesB)));
					break;

				case 'isilike':
					if(!is_array($valuesA) && !is_array($valuesB))
						array_push($this->_conditions, array('islike', $isILike_expr, array($valuesA, $valuesB)));
					break;

				case 'isin':
					if(!is_array($valuesA))
						array_push($this->_conditions, array('isin', $isIn_expr, array($valuesA, $valuesB)));
					break;

				case 'islargerthan':
					if(!is_array($valuesA) && !is_array($valuesB))
						array_push($this->_conditions, array('isin', $isLargerThan_expr, array($valuesA, $valuesB)));
					break;

				case 'islargereqthan':
					if(!is_array($valuesA) && !is_array($valuesB))
						array_push($this->_conditions, array('isin', $isLargerEqThan_expr, array($valuesA, $valuesB)));
					break;

				case 'islessthan':
					if(!is_array($valuesA) && !is_array($valuesB))
						array_push($this->_conditions, array('isin', $isLessThan_expr, array($valuesA, $valuesB)));
					break;

				case 'islesseqthan':
					if(!is_array($valuesA) && !is_array($valuesB))
						array_push($this->_conditions, array('isin', $isLessEqThan_expr, array($valuesA, $valuesB)));
					break;

				case 'isnull':
					if(!is_array($valuesA))
						array_push($this->_conditions, array('isnull', $isNull_expr, array($valuesA)));
					break;

				case 'isnotnull':
					if(!is_array($valuesA))
						array_push($this->_conditions, array('isnotnull', $isNotNull_expr, array($valuesA)));
					break;

				case 'istrue':
					if(!is_array($valuesA))
						array_push($this->_conditions, array('istrue', $isTrue_expr, array($valuesA)));
					break;

				case 'isfalse':
					if(!is_array($valuesA))
						array_push($this->_conditions, array('isfalse', $isFalse_expr, array($valuesA)));
					break;
			}
		}
		return $this;
	}

	/**
	 * Forza la generacion de la consulta SQL si se trata al objeto como String
	 */
	public function __toString() {
		return $this->parse();
	}
}

class SelectQuery extends Query {
	var $_subqueries = array();
	// Variables privadas que almacen la configuracion para generar el SQL
	var $_columns = array();
	var $_tables = array(); //tablas para unir en FROM
	var $_joins = array(); //Uniones JOIN
	var $_conditions = array(); //Condiciones para el WHERE
	var $_orders = array(); //instrucciones para el ORDER BY

	var $params = array();
	var $limits = array();

	// Alias
	var $table_alias = 't';
	var $join_alias = 'j';

	// Plantillas para generar las subsecciones de la consulta SQL
	var $select_expr = 'SELECT %s';
	var $from_expr = ' FROM %s';
	var $join_expr = ' %s';
	var $where_expr = ' WHERE %s';
	var $groupbt_expr = ' GROUP BY %s';
	var $orderby_expr = ' ORDER BY ';
	var $limit_expr = ' LIMIT %s';

	function __construct() {

	}

	/**
	 * Genera el comando SQL para ser ejecutado en el origen de datos
	 */
	function parse() {
		$select_expr_ = '';
		$from_expr_ = '';
		$join_expr_ = '';
		$wheexpr = '';
		$orderbyexpr = '';
		$limit_expr = '';

		/** parse SELECT section **/
		$columns_ = array();

		if(count($this->_columns)>0) {
			foreach($this->_columns as $col)
				array_push($columns_, sprintf('%s', $col));
		}

		$select_expr_ = sprintf($this->select_expr, (count($this->_columns)>0)?join(", ", $columns_):'*');

		/** parse FROM section **/
		$fexpr = array();
		$indx = 0;
		foreach($this->_tables as $from) {
			array_push($fexpr, sprintf("%s AS %s%d", $from, $this->table_alias, $indx++));
		}
		$from_expr_ = sprintf($this->from_expr, join(', ', $fexpr));

		/** parse JOIN section **/
		$jexpr = array();
		$indx = 0;
		foreach($this->_joins as $join) {
			array_push($jexpr, sprintf("%s JOIN %s AS %s%d ON(%s)", $join[0], $join[1], $this->join_alias, $indx++, $join[2]));
		}
		$join_expr_ = sprintf($this->join_expr, join(' ', $jexpr));

		/** parse WHERE section **/
		$where_expr_ = array();

		foreach($this->_conditions as $condition) {
			$condition_expr = $condition[1];
			$is_subquery = ($condition[2][1] instanceof SelectQuery);

			$valuesA = (is_array($condition[2][0]))?join(", ", $condition[2][0]):$condition[2][0];
			$valuesB = (is_array($condition[2][1]))?join(", ", $condition[2][1]):$condition[2][1];
			if(!!$is_subquery)
				$valuesB = '('.$condition[2][1].')';
			else
				$valuesB = (is_array($condition[2][1]))?join(", ", $condition[2][1]):$condition[2][1];

			array_push($where_expr_, sprintf($condition_expr, $valuesA, $valuesB));
		}
		$wheexpr = (count($where_expr_)==0)?'':sprintf($this->where_expr, join(' AND ', $where_expr_));

		/** parse ORDER BY section **/
		$orderbyexpr = (count($this->_orders)>0)?$this->orderby_expr.join(", ", $this->_orders):'';

		/** parse LIMIT section **/
		$limit_ = (is_array($this->limit) && count($this->limit)>0)?sprintf('%s, %s', $this->limit[0], $this->limit[1]):$this->limit;
		$limit_expr = (($this->limit!='') || (is_array($this->limit) && count($this->limit)>0))?sprintf($this->limit_expr, $limit_):'';

		//Union de SQL subsecciones
		return $select_expr_.$from_expr_.$join_expr_.$wheexpr.$orderbyexpr.$limit_expr;
	}

	//
	// Define las columnas que se mostraran de la consulta SQL
	//
	function select($columns) {
		if(is_string($columns)) array_push($this->_columns, $columns);

		foreach($columns as $column)
			array_push($this->_columns, $column);

		return $this;
	}

	//
	// Define las tablas a cruzar en el FROM de la consulta SQL
	//
	function addTable($tables) {
		if(is_string($tables)) array_push($this->_tables, $tables);

		if(is_array($tables))
			foreach($tables as $table)
				array_push($this->_tables, $table);

		return $this;
	}

	//
	// Define una union o join de la consulta SQL
	//
	function addJoin($direction = 'LEFT', $table, $condition) {
		if(in_array(strtoupper($direction), array('LEFT','RIGHT','INNER','OUTER LEFT','OUTER RIGHT','OUTER')))
			array_push($this->_joins, array($direction, $table, $condition));
		return $this;
	}

	//
	// Determina el ordenado de los resultados de la consulta SQL
	//
	function orderBy($columns, $isDescendent) {
		$order = ($isDescendent)?' DESC ':'';
		if(is_string($columns)) {
			array_push($this->_orders, $columns.$order);
			return ;
		}

		if(is_array($columns))
			foreach($columns as $column)
				array_push($this->_orders, $column.$order);

		return $this;
	}

	//
	// Limita el numero de resultados de la consulta SQL
	//
	function limit($step, $start=0) {
		$this->limit = $step;

		return $this;
	}
}

class InsertQuery extends Query {
	var $_table = '';
	var $_sets = array();
	var $_values = array();

	// Plantillas para generar las subsecciones de la consulta SQL
	var $insert_expr = 'INSERT INTO %s ';
	var $sets_expr = '(%s) ';
	var $values_expr = 'VALUES(%s) ';

	function __construct($table) {
		$this->insert($table);
	}

	function insert($table) {
		$this->_table = $table;
	}

	//
	// Define una columna y su valor para el insert
	function set($column, $value) {
		array_push($this->_sets, $column);
		array_push($this->_values, $value);

		return $this;
	}

	//
	// Define las columnas para el insert
	//
	function sets($columns) {
		foreach($columns as $column) {
			array_push($this->_sets, $column);
		}

		return $this;
	}

	//
	// Define los valores para el insert, debe corresponder a las posiciones
	// de las columnas
	//
	function values($values) {
		foreach($values as $value) {
			array_push($this->_values, $value);
		}

		return $this;
	}

	//
	// Genera el comando SQL para ser ejecutado en el origen de datos
	//
	function parse() {
		$insert_expr_ = '';
		$sets_expr_ = '';
		$values_expr_ = '';
		$where_expr = '';

		$insert_expr_ = sprintf($this->insert_expr, $this->_table);

		/** parse SET section **/
		$sets_ = array();

		if(count($this->_sets)>0) {
			foreach($this->_sets as $set)
				array_push($sets_, $set);
		}

		$set_expr_ = sprintf($this->sets_expr, join(', ', $sets_));

		/** parse VALUES section **/
		$values_ = array();

		if(count($this->_values)>0) {
			foreach($this->_values as $value)
				array_push($values_, $value);
		}

		$values_expr_ = sprintf($this->values_expr, join(', ', $values_));

		//Union de SQL subsecciones
		return $insert_expr_.$set_expr_.$values_expr_;
	}
}

class UpdateQuery extends Query {
	var $_table = '';
	var $_sets = array();
	var $_conditions = array();

	// Plantillas para generar las subsecciones de la consulta SQL
	var $update_expr = 'UPDATE %s';
	var $set_expr = ' SET %s';
	var $values_expr = ' %s=%s';
	var $where_expr = ' WHERE %s';

	function __construct($table) {
		$this->update($table);
	}

	function update($table) {
		$this->_table = $table;
	}

	function set($column, $value) {
		array_push($this->_sets, array($column, $value));

		return $this;
	}

	//
	// Genera el comando SQL para ser ejecutado en el origen de datos
	//
	function parse() {
		$table_expr_ = '';
		$set_expr_ = '';
		$where_expr = '';

		$table_expr_ = sprintf($this->update_expr, $this->_table);

		/** parse UPDATE section **/
		$sets_ = array();

		if(count($this->_sets)>0) {
			foreach($this->_sets as $set)
				array_push($sets_, sprintf($this->values_expr, $set[0], $set[1]));
		}

		$set_expr_ = sprintf($this->set_expr, join(', ', $sets_));

		/** parse WHERE section **/
		$wheres_ = array();

		foreach($this->_conditions as $condition) {
			$condition_expr = $condition[1];
			$valuesA = (is_array($condition[2][0]))?join(", ", $condition[2][0]):$condition[2][0];
			$valuesB = (is_array($condition[2][1]))?join(", ", $condition[2][1]):$condition[2][1];

			array_push($wheres_, sprintf($condition_expr, $valuesA, $valuesB));
		}
		$wheexpr_ = sprintf($this->where_expr, join(' AND ', $wheres_));

		//Union de SQL subsecciones
		return $table_expr_.$set_expr_.$wheexpr_;
	}
}

class UnitTest {
	public static function test_start() {
		self::test1();
		self::test2();
		self::test3();
		self::test4();
		self::test5();
	}

	public static function test1() {
		// SELECT
		$q1 = new SelectQuery();
		$q1->addTable(array('mos_common_contact', 'mos_medical_patient'));
		$q1->addCondition('isEqual','t0.co_id','t1.pa_co_id');
		$q1->addCondition('isEqual','t0.co_id', 1000);
		// $q1->addFacade('mysql');
		// $q1->connect('prueba','root','root');
		$sql = $q1->parse();
		echo sprintf("sql:%s<br/>", $sql);
	}

	public static function test2() {
		// SELECT
		$q2 = new SelectQuery();
		$q2->select(array('t0.co_id','t1.pa_co_id','t0.co_lastname'));
		$q2->addTable(array('mos_common_contact', 'mos_medical_patient'));
		$q2->addCondition('isEqual','t0.co_id','t1.pa_co_id');
		$q2->addCondition('isEqual','t0.co_id','10000');
		$q2->addCondition('isLike','t0.co_id','8xovfig');
		$q2->addCondition('isIlike','co_lastname',"'a%'");
		$q2->addCondition('isIlike','co_lastname',"'%z'");
		$q2->addCondition('isIn',"'z'",array('co_lastname','co_firstname'));
		$q2->addCondition('isFalse',"w=100");
		$q2->addCondition('isNULL',"wash");
		$q2->orderBy('t0.co_lastname', true);
		$q2->limit(10);
		$sql = $q2->parse();
		echo sprintf("sql:%s<br/>", $sql);
	}

	public static function test3() {
		// SELECT
		$q3 = new SelectQuery();
		$q3->select(array('t0.co_id','t1.pa_co_id','t0.co_lastname'));
		$q3->addTable(array('mos_common_contact', 'mos_medical_patient'));
		$q3->addJoin('LEFT','Prueba','j0.a = j1.b');
		$q3->addJoin('RIGHT','Prueba','j0.a = j1.b');
		$q3->orderBy('t0.co_lastname', true);
		$q3->limit(10);
		$sql = $q3->parse();
		echo sprintf("sql:%s<br/>", $sql);
	}

	public static function test4() {
		// UPDATE
		$update = new UpdateQuery('mos_common_contact');
		$update->set('cedula','14008387');
		$update->set('empleado','"Pedro"');
		$update->addCondition('isEqual','t0.co_id','t1.pa_co_id');
		$sql = $update->parse();
		echo sprintf("sql:%s<br/>", $sql);
	}

	public static function test5() {
		$insert = new InsertQuery('contacto');
		$insert->sets(array('cedula','nombres','apellidos'));
		$insert->values(array(14008387,'"Soulberto"','"Lorenzo"'));
		$insert->set('nivel', 5);
		$sql = $insert->parse();
		echo sprintf("sql:%s<br/>", $sql);
	}

	public static function test6() {
		$inner_select = new SelectQuery();
		$inner_select->select(array('su_systemaccount'))
			->addTable(array('mos_security_user'))
			->addJoin('LEFT', 'mos_system_account', 'sa_id = su_systemaccount')
			->addCondition('ISEQUAL', 'uid', '41420e4bafd63496')
			->limit(1);


		$select = new SelectQuery();
		$select->addTable(array('mos_medical_doctor'))
			->addJoin('LEFT', 'mos_common_contact', 'md_co_id=co_id')
			->addJoin('LEFT', 'mos_security_user', 'uid = md_owner')
			->addCondition('ISEQUAL', 'su_systemaccount', $inner_select)
			->addCondition('ISFALSE', 'md_deleted');

		echo $select;
	}

	public static function test7() {
		$insert = new InsertQuery('payments');
		$insert->sets(array('id', 'value'))
			->values(array(1, 1000));

		echo $insert;
	}
}

if(basename(__FILE__) == 'dbquery.php') {
	echo '<code>';
	// UnitTest::test_start();
	UnitTest::test7();
	echo '</code>';
}
?>